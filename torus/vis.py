#!/usr/bin/env python

# Plot (projections of) dynamics

import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys

# Read data
with open(sys.argv[1], 'rb') as f:
    history = pickle.load(f)

t = np.array([h[0] for h in history])
x = np.array([h[1] for h in history])

plt.figure()
#plt.plot(t, x[:,0])
plt.plot(x[:,0], x[:,1])
plt.show()
